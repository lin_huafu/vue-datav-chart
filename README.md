<!--
 * @Author: tao
 * @Date: 2020-09-21 17:11:26
 * @LastEditTime: 2020-09-21 17:32:03
 * @LastEditors: Please set LastEditors
 * @Description: 项目介绍
 * @FilePath: tao-icon\README.en.md
-->

#### 介绍

基于Vue3.0+echarts 构建的数据可视化大屏模板,只是模板一切数据都是假的。

#### 使用说明

基本图表组件:

```html
<tao-charts :data="options" />

options:{} // 参考echarts 配置

```

##### 组件说明

项目做了自动注册全局组件,新增一个全局组件可直接在components下新增就可以了,但要注意需要指名name

效果展示：
![](http://mindoc.njapld.com/uploads/markdown/images/m_7d694e02e98423482b84a87f38203b00_r.png)
![](http://mindoc.njapld.com/uploads/markdown/images/m_065909a54b4fef48abe560120fb49f32_r.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
