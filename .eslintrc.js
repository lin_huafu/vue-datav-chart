/*
 * @Author: your name
 * @Date: 2020-10-12 12:53:58
 * @LastEditTime: 2020-10-12 13:22:04
 * @LastEditors: Please set LastEditors
 * @Description: Eslint 规则
 */
module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/vue3-essential", "@vue/airbnb"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "linebreak-style": [0, "error", "windows"],
    "import/no-named-as-default": 0,
    "comma-dangle": ["error", {
      "arrays": "never",
      "objects": "never",
      "imports": "never",
      "exports": "never",
      "functions": "never"
    }],
    //强制驼峰法命名
    camelcase: [
      0,
      {
        properties: "always"
      }
    ],
    quotes: [2, "double"], //引号类型 `` "" ''
    semi: [2, "never"], //语句强制分号结尾
    "comma-style": [2, "last"] // 控制逗号在行尾出现还是在行首出现
  },
  overrides: [
    {
      files: ["**/__tests__/*.{j,t}s?(x)", "**/tests/unit/**/*.spec.{j,t}s?(x)"],
      env: {
        mocha: true
      }
    }
  ]
};
