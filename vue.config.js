const publicPath = process.env.NODE_ENV === "development" ? "/" : "./."
module.exports = {
  publicPath,
  lintOnSave: true,
  outputDir: "dist",
  assetsDir: "static",
  productionSourceMap: false,
  chainWebpack: (config) => {
    config.plugins.delete("prefetch").delete("preload")
  },
  configureWebpack: {
    externals: {
      AMap: "AMap"
    }
  }
}
