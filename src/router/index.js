/*
 * @Author: your name
 * @Date: 2020-10-12 12:53:58
 * @LastEditTime: 2020-10-19 16:11:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 */
import { createRouter, createWebHashHistory } from "vue-router"
import Home from "@/views/Home.vue"

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/echartMap", // echarts 下钻
    name: "echartMap",
    component: () => import("@/views/demo")
  },
  {
    path: "/map",
    name: "map",
    component: () => import("@/views/demo/map.vue")
  },
  {
    path: "/map2", // 高德地图下钻
    name: "map2",
    component: () => import("@/views/demo/map2.vue")
  },
  {
    path: "/SmartCommunity",
    name: "SmartCommunity",
    component: () => import("@/views/chartDemo/SmartCommunity")
  },
  {
    path: "/LandBigData",
    name: "LandBigData",
    component: () => import("@/views/chartDemo/LandBigData")
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  scrollBehavior: () => ({
    y: 0
  }),
  routes
})

export default router
