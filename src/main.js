/*
 * @Author: your name
 * @Date: 2020-10-12 12:53:58
 * @LastEditTime: 2020-10-12 13:29:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 */
import { createApp } from "vue"

// 将自动注册所有组件为全局组件
import dataV from "@jiaminghi/data-view"
import RegGlobComp from "@/components"
// eslint-disable-next-line import/no-unresolved
import utils from "@/utils"
import App from "./App.vue"
import router from "./router"
import store from "./store"

const echarts = require("echarts")

const app = createApp(App)
app.config.globalProperties.$utils = utils
app.config.globalProperties.$echarts = echarts
RegGlobComp(app) // 挂载全局组件
app
  .use(dataV)
  .use(store)
  .use(router)
  .mount("#app")
