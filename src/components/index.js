/*
 * @Author: your name
 * @Date: 2020-10-12 12:54:30
 * @LastEditTime: 2020-10-12 13:26:06
 * @LastEditors: Please set LastEditors
 * @Description: 全局组件自动注册
 */

const vueFiles = require.context("/", true, /component\.vue$/)
const jsFiles = require.context("/", true, /component\.js$/)

export function GlobComp(app) {
  vueFiles.keys().forEach((key) => {
    const component = vueFiles(key).default
    app.component(component.name, component)
  })
  jsFiles.keys().forEach((key) => {
    const component = jsFiles(key).default
    app.component(component.name, component)
  })
}
export default GlobComp
