/* eslint-disable no-plusplus */

import { getGeoJson } from "@/utils"

const echarts = require("echarts")

export function mapCharts(titleName, abcde = "100000", data) {
  return new Promise((resolve, reject) => {
    getGeoJson(abcde).then((res) => {
      if (res.features.length > 0) {
        echarts.registerMap(titleName, res)
        const mapData = res.data
        const centerMap = {}
        if (mapData && mapData.features && mapData.features.length > 0) {
          for (let i = 0; i < mapData.features.length; i++) {
            const feature = mapData.features[i]
            const properties = feature.properties || {}
            if (properties && properties.name) {
              centerMap[properties.name] = properties.center || []
            }
          }
        }
        const labels = {
          normal: {
            show: true,
            formatter(params) {
              console.log(params, "sssss")
              return `{fline|${params.seriesName}}\n{tline|${params.data.name}}${params.data.value[2]}`
            },
            position: "top",
            backgroundColor: "rgba(254,174,33,.8)",
            padding: [0, 0],
            borderRadius: 3,
            lineHeight: 32,
            color: "#f7fafb",
            rich: {
              fline: {
                padding: [0, 10, 10, 10],
                color: "#ffffff"
              },
              tline: {
                padding: [0, 10],
                color: "#ffffff"
              }
            }
          },
          emphasis: {
            show: true
          }
        }
        const optionMap = {
          backgroundColor: "#4f5555",
          grid: {
            // left:'2%',
            right: "10%",
            bottom: "3%",
            width: "20%",
            height: "50%"
          },
          geo: {
            show: true,
            zoom: 3, // 默认显示级别
            map: titleName,
            label: {
              normal: {
                color: "#fff",
                show: true
              },
              emphasis: {
                show: false
              }
            },
            roam: true,
            itemStyle: {
              show: false,
              normal: {
                opacity: 0.4,
                areaColor: "rgba(122,193,254,0.2)",
                borderColor: "#1c89cd",
                borderWidth: 2
              },
              emphasis: { // 鼠标移动上去变色
                areaColor: "#4695f2",
                textStyle: {
                  show: 0
                }
              }
            }
          },
          series: [
            {
              name: "机构",
              type: "effectScatter",
              coordinateSystem: "geo",
              symbolSize: 15,
              symbol: "pin", // 气泡
              label: {
                ...labels
              },
              itemStyle: {
                normal: {
                  color: "#FFC600" // 标志颜色
                }
              },
              zlevel: 6,
              data: data.data2
            },
            {
              name: "服务组织",
              type: "effectScatter",
              coordinateSystem: "geo",
              symbol: "pin", // 气泡
              symbolSize: 15,
              label: {
                ...labels
              },
              itemStyle: {
                normal: {
                  color: "#F62157" // 标志颜色
                }
              },
              zlevel: 6,
              data: data.data3
            }
          ]
        }
        resolve(optionMap)
      }
    }).catch((err) => {
      reject(err)
    })
  })
}

export default {
  mapCharts
}
