const echarts = require("echarts")

const lineChart = (datasource) => {
  const options = {
    legend: {
      x: "center",
      y: "15%",
      textStyle: {
        color: "#4ADEFE"
      }
    },
    grid: {
      left: "12%",
      top: "25%",
      bottom: "12%",
      right: "8%"
    },
    tooltip: {},
    dataset: {
      source: datasource
    },
    xAxis: {
      type: "category",
      axisLine: {
        show: false,
        lineStyle: {
          color: "#4ADEFE"
        }
      },
      // data: ["岳阳市","益阳市","长沙市","株洲市","衡阳市","永州市","娄底市","郴州市","湘潭市"],
      axisTick: {
        alignWithLabel: true
      }
    },
    yAxis: {
      axisLine: {
        lineStyle: {
          color: "#4ADEFE"
        }
      },
      splitLine: {
        lineStyle: {
          color: "#4ADEFE"
        }
      }
    },
    series: [
      {
        type: "bar",
        barMaxWidth: "10",
        itemStyle: {
          normal: {
            color: "#4ADEFE"
          }
        }
      },
      {
        type: "bar",
        barMaxWidth: "10",
        itemStyle: {
          normal: {
            color: "#F3DB5C"
          }
        }
      }
    ]
  }
  return options
}

const quanChart = () => {
  const option = {
    title: [{
      text: "确权总面积",
      left: "17.5%",
      top: "60%",
      textAlign: "center",
      textStyle: {
        fontWeight: "normal",
        fontSize: 12,
        color: "#fff",
        textAlign: "center"
      }
    }, {
      text: "流转总面积",
      left: "49.5%",
      top: "60%",
      textAlign: "center",
      textStyle: {
        color: "#fff",
        fontWeight: "normal",
        fontSize: 12,
        textAlign: "center"
      }
    }, {
      text: "挂网总面积",
      left: "81.5%",
      top: "60%",
      textAlign: "center",
      textStyle: {
        color: "#fff",
        fontWeight: "normal",
        fontSize: 12,
        textAlign: "center"
      }
    }],
    series: [{
      type: "pie",
      radius: ["30%", "35%"],
      center: ["18%", "50%"],
      startAngle: 225,
      color: [new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
        offset: 0,
        color: "#3e9def"
      }, {
        offset: 1,
        color: "#0356e2"
      }]), "transparent"],
      labelLine: {
        normal: {
          show: false
        }
      },
      label: {
        normal: {
          position: "center"
        }
      },
      data: [{
        value: 75,
        name: "",
        label: {
          normal: {
            formatter: "35%",
            textStyle: {
              color: "#0355df",
              fontSize: 25
            }
          }
        }
      }, {
        value: 25,
        name: "%"

      }]
    },
    {
      name: "信息",
      type: "pie",
      radius: ["30%", "35%"],
      center: ["50%", "50%"],
      startAngle: 225,
      color: [new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
        offset: 0,
        color: "#5ffe70"
      }, {
        offset: 1,
        color: "#41e350"
      }]), "transparent"],
      labelLine: {
        normal: {
          show: false
        }
      },
      label: {
        normal: {
          position: "center"
        }
      },
      data: [{
        value: 75,
        name: "信息",
        label: {
          normal: {
            formatter: "",
            textStyle: {
              color: "#fff",
              fontSize: 16
            }
          }
        }
      }, {
        value: 25,
        name: "%",
        label: {
          normal: {
            formatter: "3534%",
            textStyle: {
              color: "#3de351",
              fontSize: 25

            }
          }
        }
      }
      ]
    },
    {
      name: "数量",
      type: "pie",
      radius: ["30%", "35%"],
      center: ["82%", "50%"],
      startAngle: 225,
      labelLine: {
        normal: {
          show: false
        }
      },
      label: {
        normal: {
          position: "center"
        }
      },
      data: [{
        value: 75,
        itemStyle: {
          normal: {
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: "#ffad33"
            }, {
              offset: 1,
              color: "#ff8b04"
            }])
          }
        },
        name: "数量",
        label: {
          normal: {
            formatter: "",
            textStyle: {
              color: "#fff",
              fontSize: 12
            }
          }
        }
      }, {
        value: 25,
        name: "%",
        label: {
          normal: {
            formatter: "3534%",
            textStyle: {
              color: "#ff8a06",
              fontSize: 25
            }
          }
        }
      },
      {
        value: 0,
        name: "%"
      }
      ]
    }
    ]
  }
  return option
}
const mapCharts = (datas) => {
  console.log(echarts, "echartsechartsecharts")
  // 引入北京地区地图
  echarts.registerMap("北京", datas)
  // 每个地区的坐标
  const geoCoordMap = {
    东城: [116.424697, 39.927967],
    西城: [116.358141, 39.913919],
    朝阳: [116.765487, 40.136573],
    丰台: [116.290679, 39.857184],
    石景山: [116.17672, 39.949198],
    海淀: [116.177807, 40.062966],
    门头沟: [115.905234, 40.010063],
    房山: [115.941902, 39.708825],
    通州: [116.747351, 39.814339],
    顺义: [116.737316, 40.137897],
    昌平: [116.226118, 40.225311],
    大兴: [116.402095, 39.655493],
    怀柔: [116.621138, 40.432762],
    平谷: [117.117604, 40.192158],
    密云: [117.065719, 40.500122],
    延庆: [116.016705, 40.507607]
  }

  // 每个地区的累计疫情数量值
  const data = [{
    name: "东城",
    value: 390,
    alarm_num: 54
  }, {
    name: "西城",
    value: 119
  }, {
    name: "朝阳",
    value: 55,
    alarm_num: 9
  }, {
    name: "丰台",
    value: 329
  }, {
    name: "石景山",
    value: 219,
    alarm_num: 14
  }, {
    name: "海淀",
    value: 290
  }, {
    name: "门头沟",
    value: 319,
    alarm_num: 2
  }, {
    name: "房山",
    value: 199
  }, {
    name: "通州",
    value: 419,
    alarm_num: 11
  }, {
    name: "顺义",
    value: 299
  }, {
    name: "昌平",
    value: 49
  }, {
    name: "大兴",
    value: 219,
    alarm_num: 15
  }, {
    name: "怀柔",
    value: 89
  }, {
    name: "平谷",
    value: 49
  }, {
    name: "密云",
    value: 209,
    alarm_num: 27
  }, {
    name: "延庆",
    value: 129
  }]

  // 每个地区的新增疫情数量
  const data3 = [{
    name: "通州",
    value: 54
  }, {
    name: "顺义",
    value: 9
  }, {
    name: "平谷",
    value: 14
  }, {
    name: "丰台",
    value: 2
  }, {
    name: "怀柔",
    value: 11
  }]

  const convertData = (datass) => {
    const res = []
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < datass.length; i++) {
      const geoCoord = geoCoordMap[datass[i].name]
      if (geoCoord) {
        res.push({
          name: datass[i].name,
          value: geoCoord.concat(datass[i].value)
        })
      }
    }
    // 有数据的地区的名称和value值
    return res
  }

  // 设定series是一个空的数组
  const series = [];
  [
    ["北京", data]
  ].forEach((item) => {
    series.push({
      name: "定位点",
      type: "effectScatter", // 散点图
      coordinateSystem: "geo", // 使用地理坐标系
      hoverAnimation: "false",
      legendHoverLink: "false",
      rippleEffect: {
        period: 4,
        brushType: "stroke",
        scale: 3
      },
      data: convertData(item[1]),
      // eslint-disable-next-line consistent-return
      symbolSize(val) {
        if (val[2] <= 100) {
          return 4
        }
        if (val[2] > 100 && val[2] <= 200) {
          return 8
        }
        if (val[2] > 200 && val[2] <= 300) {
          return 12
        }
        if (val[2] > 300 && val[2] <= 400) {
          return 16
        }
        if (val[2] > 400 && val[2] <= 500) {
          return 20
        }
        if (val[2] > 500) {
          return 24
        }
      },
      itemStyle: {
        normal: {
          color: "#5599E4"
        }
      },
      zlevel: 1
    }, {
      type: "map",
      map: "北京",
      zlevel: 0,
      itemStyle: {
        normal: {
          areaColor: "#409EFF",
          borderColor: "#fff"
        },
        emphasis: {
          areaColor: "#2a333d"
        }
      },
      label: {
        normal: {
          show: true,
          textStyle: {
            color: "#303133",
            fontSize: 10
          }
        },
        emphasis: {
          textStyle: {
            color: "#ddb926",
            fontSize: 10
          }
        }

      },
      data
    }, {
      name: "点",
      type: "effectScatter",
      coordinateSystem: "geo",
      // symbol: 'pin',
      symbolSize: 15,
      label: {
        normal: {
          show: true,
          textStyle: {
            color: "#E6A23C",
            fontSize: 9
          }
        }
      },
      itemStyle: {
        normal: {
          color: "#F62157" // 标志颜色
        }
      },
      zlevel: 6,
      data: convertData(data3)
    })
  })

  const option = {
    geo: {
      map: "北京",
      label: {
        textStyle: {
          fontSize: ".1rem"
        }
      },
      itemStyle: {
        normal: {
          areaColor: "#fff",
          borderColor: "#587A9F"
        },
        emphasis: {
          areaColor: "#ccc"
        }
      }
    },
    series
  }
  return option
}

const pieChart = (datasource) => {
  const option = {
    color: ["#cd4692", "#9658c3", "#6c6be2", "#01aebf", "#18b794"],
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/> {c}({d}%)",
      textStyle: {
        fontSize: 16
      }
    },
    dataset: {
      source: datasource
    },
    series: [{
      type: "pie",
      clockwise: false,
      startAngle: 90,
      radius: "45%",
      center: ["50%", "50%"],
      hoverAnimation: false,
      roseType: "radius", // area
      itemStyle: {
        normal: {
          borderColor: "#273454",
          borderWidth: "3"
        }
      },
      label: {
        show: true,
        position: "outside",
        formatter: "{a|{b}：{d}%}\n{hr|}",
        rich: {
          hr: {
            backgroundColor: "t",
            borderRadius: 100,
            width: 0,
            height: 5,
            padding: [3, 3, 0, -16],
            shadowColor: "#1c1b3a",
            shadowBlur: 1,
            shadowOffsetX: "0",
            shadowOffsetY: "2"
          },
          a: {
            padding: [-35, 15, -20, 5]
          }
        }
      },
      labelLine: {
        normal: {
          length: 10,
          length2: 20,
          lineStyle: {
            width: 1
          }
        }
      }
    }]
  }
  return option
}

export default {
  lineChart,
  quanChart,
  mapCharts,
  pieChart
}
