const echarts = require("echarts")

const lineChart = (datasource) => {
  const options = {
    grid: {
      left: "2%",
      top: "30%",
      bottom: "5%",
      right: "8%",
      containLabel: true
    },
    tooltip: {},
    xAxis: {
      type: "category",
      axisTick: {
        alignWithLabel: true
      },
      nameTextStyle: {
        color: "#82b0ec"
      },
      axisLine: {
        show: false,
        lineStyle: {
          color: "#82b0ec"
        }
      },
      axisLabel: {
        textStyle: {
          color: "#fff"
        },
        margin: 30
      }
    },
    yAxis: {
      show: false,
      gridIndex: 0,
      axisLine: {
        lineStyle: {
          color: "#4ADEFE"
        }
      },
      splitLine: {
        lineStyle: {
          color: "#4ADEFE"
        }
      }
    },
    dataset: {
      source: datasource
    },
    series: [{
      name: "",
      type: "pictorialBar",
      symbolSize: [20, 10],
      symbolOffset: [0, -6],
      symbolPosition: "end",
      z: 12,
      // "barWidth": "0",
      label: {
        normal: {
          show: true,
          position: "top",
          // "formatter": "{c}%"
          fontSize: 18,
          fontWeight: "bold",
          color: "#34DCFF"
        }
      },
      color: "#2DB1EF"
    },
    {
      type: "pictorialBar",
      symbolSize: [20, 10],
      symbolOffset: [0, 7],
      // "barWidth": "20",
      z: 12,
      color: "#2DB1EF"
    },
    {
      type: "pictorialBar",
      symbolSize: [30, 15],
      symbolOffset: [0, 12],
      z: 10,
      itemStyle: {
        normal: {
          color: "transparent",
          borderColor: "#2EA9E5",
          borderType: "solid",
          borderWidth: 1
        }
      }
    },
    {
      type: "pictorialBar",
      symbolSize: [40, 20],
      symbolOffset: [0, 18],
      z: 10,
      itemStyle: {
        normal: {
          color: "transparent",
          borderColor: "#19465D",
          borderType: "solid",
          borderWidth: 2
        }
      }
    },
    {
      type: "bar",
      barWidth: "20",
      barGap: "10%", // Make series be overlap
      barCateGoryGap: "10%",
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 0.7, [{
            offset: 0,
            color: "#38B2E6"
          },
          {
            offset: 1,
            color: "#0B3147"
          }
          ]),
          opacity: 0.8
        }
      }
    }
    ]
  }
  return options
}
const mapChart = (datas) => {
  echarts.registerMap("china", datas)
  const data = [
    {
      name: "北京",
      transAmt: 199,
      serviceRate: 99
    },
    {
      name: "苏州",
      transAmt: 42,
      serviceRate: 99
    },
    {
      name: "南京",
      transAmt: 102,
      serviceRate: 99
    },
    {
      name: "上海",
      transAmt: 81,
      serviceRate: 99
    },
    {
      name: "杭州",
      transAmt: 47,
      serviceRate: 99
    },
    {
      name: "天津",
      transAmt: 67,
      serviceRate: 99
    },
    {
      name: "成都",
      transAmt: 82,
      serviceRate: 99
    },
    {
      name: "宁波",
      transAmt: 123,
      serviceRate: 99
    },
    {
      name: "上海",
      transAmt: 24,
      serviceRate: 99
    },
    {
      name: "深圳",
      transAmt: 92,
      serviceRate: 99
    }
  ]
  const option = {
    tooltip: {
      trigger: "item",
      formatter(params) {
        if (typeof params.value[2] === "undefined") {
          return `${params.name} : ${params.value}`
        }
        return `${params.name} : ${params.value[2]}`
      }
    },
    legend: {
      orient: "vertical",
      y: "bottom",
      x: "right",
      data: ["pm2.5"],
      textStyle: {
        color: "#fff"
      }
    },
    visualMap: {
      show: false,
      min: 0,
      max: 500,
      left: "left",
      top: "bottom",
      text: ["高", "低"], // 文本，默认为数值文本
      calculable: true,
      seriesIndex: [1],
      inRange: {}
    },
    geo: {
      map: "china",
      show: true,
      roam: true,
      label: {
        normal: {
          show: false
        },
        emphasis: {
          show: false
        }
      },
      itemStyle: {
        normal: {
          areaColor: "#3a7fd5",
          borderColor: "#0a53e9", // 线
          shadowColor: "#092f8f", // 外发光
          shadowBlur: 20
        },
        emphasis: {
          areaColor: "#0a2dae" // 悬浮区背景
        }
      }
    },
    series: [
      {
        symbolSize: 5,
        label: {
          normal: {
            formatter: "{b}",
            position: "right",
            show: true
          },
          emphasis: {
            show: true
          }
        },
        itemStyle: {
          normal: {
            color: "#fff"
          }
        },
        name: "light",
        type: "scatter",
        coordinateSystem: "geo",
        data: [...data]
      },
      {
        type: "map",
        map: "china",
        geoIndex: 0,
        aspectScale: 0.75, // 长宽比
        showLegendSymbol: false, // 存在legend时显示
        label: {
          normal: {
            show: false
          },
          emphasis: {
            show: false,
            textStyle: {
              color: "#fff"
            }
          }
        },
        roam: true,
        itemStyle: {
          normal: {
            areaColor: "#031525",
            borderColor: "#FFFFFF"
          },
          emphasis: {
            areaColor: "#2B91B7"
          }
        },
        animation: false,
        data
      },
      {
        name: "Top 5",
        type: "scatter",
        coordinateSystem: "geo",
        symbol: "pin",
        symbolSize: [50, 50],
        label: {
          normal: {
            show: true,
            textStyle: {
              color: "#fff",
              fontSize: 9
            },
            formatter(value) {
              return value.data.value[2]
            }
          }
        },
        itemStyle: {
          normal: {
            color: "#D8BC37" // 标志颜色
          }
        },
        data: [...data],
        showEffectOn: "render",
        rippleEffect: {
          brushType: "stroke"
        },
        hoverAnimation: true,
        zlevel: 1
      }
    ]
  }
  return option
}

const pieChart = (datasource) => {
  const option = {
    color: ["#cd4692", "#9658c3", "#6c6be2", "#01aebf", "#18b794"],
    tooltip: {
      trigger: "item",
      axisPointer: {
        type: "shadow"
      },
      formatter: "{a} <br/>{c}({d}%)",
      textStyle: {
        fontSize: 16
      }
    },
    dataset: {
      source: datasource
    },
    series: [{
      type: "pie",
      clockwise: false,
      startAngle: 90,
      radius: "45%",
      center: ["50%", "50%"],
      roseType: "radius", // area
      itemStyle: {
        normal: {
          borderColor: "#273454",
          borderWidth: "3"
        }
      },
      label: {
        show: true,
        position: "outside",
        formatter: "{a|{b}：{d}%}\n{hr|}",
        rich: {
          hr: {
            backgroundColor: "t",
            borderRadius: 100,
            width: 0,
            height: 5,
            padding: [3, 3, 0, -16],
            shadowColor: "#1c1b3a",
            shadowBlur: 1,
            shadowOffsetX: "0",
            shadowOffsetY: "2"
          },
          a: {
            padding: [-35, 15, -20, 5]
          }
        }
      },
      labelLine: {
        normal: {
          length: 10,
          length2: 20,
          lineStyle: {
            width: 1
          }
        }
      }
    }]
  }
  return option
}

const pieChartVie = () => {
  const scaleData = [{
    name: "红灯 14",
    value: 14,
    radius1: [58, 60],
    radius2: "25%"
  }, {
    name: "黄灯 32",
    value: 32,
    radius1: [80, 82],
    radius2: "30%"
  }, {
    name: "绿灯 288",
    value: 288,
    radius1: [102, 104],
    radius2: "35%"
  }, {
    name: "挂起 463",
    value: 463,
    radius1: [124, 126],
    radius2: "40%"
  }
  ]
  const placeHolderStyle = {
    normal: {
      label: {
        show: false
      },
      labelLine: {
        show: false
      },
      color: "rgba(0, 0, 0, 0)",
      borderColor: "rgba(0, 0, 0, 0)",
      borderWidth: 0
    }
  }
  const seriesObj = []
  const color = ["#FF647C", "#FFBE75", "#3EE2A5", "#6C77FD"]
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < scaleData.length; i++) {
    console.log(scaleData[i].name)
    seriesObj.push({
      name: "",
      type: "pie",
      radius: scaleData[i].radius1,
      hoverAnimation: false,
      itemStyle: {
        normal: {
          label: {
            show: false,
            color: "#ddd"
          }
        }
      },
      data: [{
        value: scaleData[i].value,
        name: scaleData[i].name,
        itemStyle: {
          normal: {
            borderWidth: 5,
            borderColor: color[i]
          }
        }
      }, {
        value: 200,
        name: "",
        itemStyle: placeHolderStyle
      }]
    }, {
      name: "",
      type: "gauge",
      detail: false,
      splitNumber: 10, // 刻度数量
      radius: scaleData[i].radius2, // 图表尺寸
      center: ["50%", "50%"],
      startAngle: 0, // 开始刻度的角度
      endAngle: -356, // 结束刻度的角度
      axisLine: {
        show: false,
        lineStyle: {
          width: 0,
          shadowBlur: 0
        }
      },
      axisTick: {
        show: true,
        lineStyle: {
          color: "rgba(220,220,220,0.5)",
          width: 5
        },
        length: 5,
        splitNumber: 5
      },
      splitLine: {
        show: false,
        length: 5,
        lineStyle: {
          color: "rgba(220,220,220,0.1)"
        }
      },
      axisLabel: {
        show: false
      }
    })
  }
  const option = {
    color,
    tooltip: {
      show: false
    },
    legend: {
      orient: "vertical",
      x: "200",
      y: "center",
      itemGap: 35,
      data: ["挂起 463", "红灯 14", "黄灯 32", "绿灯 288"],
      show: true,
      textStyle: {
        color: "#fff"
      }
    },
    toolbox: {
      show: false
    },
    series: seriesObj
  }
  return option
}

const hillChart = (datasource) => {
  const option = {
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow"
      }
    },
    grid: {
      left: "12%",
      top: "25%",
      bottom: "12%",
      right: "8%"
    },
    xAxis: {
      type: "category",
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: "rgba(255, 129, 109, 0.1)",
          width: 1 // 这里是为了突出显示加上的
        }
      },
      axisLabel: {
        textStyle: {
          color: "#999",
          fontSize: 12
        }
      }
    },
    yAxis: [{
      splitNumber: 2,
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: "rgba(255, 129, 109, 0.1)",
          width: 1 // 这里是为了突出显示加上的
        }
      },
      axisLabel: {
        textStyle: {
          color: "#999"
        }
      },
      splitArea: {
        areaStyle: {
          color: "rgba(255,255,255,.5)"
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: "rgba(255, 129, 109, 0.1)",
          width: 0.5,
          type: "dashed"
        }
      }
    }],
    dataset: {
      source: datasource
    },
    series: [{
      type: "pictorialBar",
      barCategoryGap: "0%",
      symbol: "path://M0,10 L10,10 C5.5,10 5.5,5 5,0 C4.5,5 4.5,10 0,10 z",
      label: {
        show: true,
        position: "top",
        distance: 10,
        color: "#b73a35",
        fontWeight: "bolder",
        fontSize: 14
      },
      itemStyle: {
        normal: {
          color: {
            type: "linear",
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: "#5433f7" //  0%  处的颜色
            },
            {
              offset: 1,
              color: "#f24c44" //  100%  处的颜色
            }
            ],
            global: false //  缺省为  false
          }
        },
        emphasis: {
          opacity: 1
        }
      },
      z: 10
    }]
  }
  return option
}

const barChart = (datasource) => {
  const option = {
    color: ["#FADB71"],
    tooltip: {
      trigger: "axis",
      axisPointer: { // 坐标轴指示器，坐标轴触发有效
        type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    grid: {
      left: "12%",
      top: "25%",
      bottom: "12%",
      right: "8%"
    },
    xAxis: [
      {
        type: "category",
        axisTick: {
          alignWithLabel: true
        },
        axisLabel: {
          color: "#FADB71" // 刻度线标签颜色
        }
      }
    ],
    yAxis: [
      {
        type: "value",
        axisLabel: {
          color: "#FADB71" // 刻度线标签颜色
        }
      }
    ],
    dataset: {
      source: datasource
    },
    series: [
      {
        type: "bar",
        barWidth: "60%"
      }
    ]
  }
  return option
}
export default {
  lineChart,
  mapChart,
  pieChart,
  pieChartVie,
  hillChart,
  barChart
}
